package se.arbetsformedlingen.iris.springrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SpringRestApplication {


	@GetMapping(path = "/")
	public String hello() {
		return "Hello Jenkins X World feature 1";
	}

	public static void main(String[] args) {

		SpringApplication.run(SpringRestApplication.class, args);
	}
}
